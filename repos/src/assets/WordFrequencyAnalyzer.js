export default class WordFrequencyAnalyzer {
  constructor (text) {
    this.text = text
  }

  calculateHighestFrequency () {
    const wordFreqObj = {}
    this.text.split(/[^\w]/g).forEach(word => {
      word = word.toLowerCase()
      wordFreqObj[word] = wordFreqObj[word] ? wordFreqObj[word] + 1 : 1
    })
    return Object.values(wordFreqObj).sort().reverse()[0]
  } // return number

  calculateFrequencyForWord (inptWord = '') {
    const wordFreqObj = {}
    // const reg = new RegExp('\\B' + word + '\\B')
    this.text.split(/[^\w]/g).forEach(word => {
      if (word === inptWord) {
        word = word.toLowerCase()
        wordFreqObj[word] = wordFreqObj[word] ? wordFreqObj[word] + 1 : 1
      }
    })
    return Object.values(wordFreqObj).sort().reverse()[0]
  } // return number

  calculateMostFrequentNWords (n = 0) {
    this.text.split(/[^\w]/g).forEach(word => {
      word = word.toLowerCase()
      if (word.match('n')) {
        n++
      }
    })
    return n
  } // return array
}
